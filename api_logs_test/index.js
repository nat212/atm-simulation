function jsonParse(res)
{
    return res.json();
}

const request = (url, method, start, end) => new Promise((resolve, reject) => {
    fetch(`${url}?startDate=${start}&endDate=${end}`)
    .then(jsonParse)
    .then(data => {
        resolve(data);
    });
});

let counter = 0;
let total = 0;
request('https://atm.draper.net.za/api/logs', "GET", "2019-03-10", "2019-03-14").then(data => {
    assertEquals(data[0].response_code , 0);
    assertEquals(data[0].url, "string");
    
    assertEqualsEmpty(data[0].request_body, {});
    assertEqualsEmpty(data[0].response_body, {});
    
    //document.getElementById('result').innerHTML = `${counter} tests have been passed`;

});





function assertEquals(input, expected)
{
    
    total++;
    if(input == expected)
    {
        counter++;
       
        console.log("Test passed.");
        
    }
    else{
        console.log(`Test ${total} failed`);
    }

    document.getElementById('result').innerHTML = `${counter} / ${total} tests have been passed\n`;
        

}

function assertEqualsEmpty(input, expected)
{
    
    total++;
    if((Object.entries(input).length === 0 && input.constructor === Object) && (Object.entries(expected).length === 0 && expected.constructor === Object))
    {
        counter++;
       
        console.log("Test passed.");
        
    }
    else{
        console.log(`Test ${total} failed`);
    }

    document.getElementById('result').innerHTML = `${counter} / ${total} tests have been passed\n`;
        

}