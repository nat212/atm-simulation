package com.fnb.atmsimulation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;

import org.json.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class StatementActivity extends AppCompatActivity
{

    private String accountID;
    private String accountName;
    public TextView balanceText;
    public TextView displayBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statement);

        balanceText = (TextView) findViewById(R.id.balanceText);
        displayBalance = (TextView) findViewById(R.id.displayBalance);

        Intent intent = getIntent();
        accountID = intent.getStringExtra("accountID");
        accountName = intent.getStringExtra("accountName");

        TextView heading = findViewById(R.id.statementHeading);
        heading.setText("Mini-Statement of " + accountName);

        try
        {
            getAndDisplayStatementInfo1(accountID);
            getAndDisplayStatementInfo2(accountID);
        }
        catch (JSONException e)
        {
            Log.e("Error", "JSONException has occured: " + e);
        }
    }

    public void getAndDisplayStatementInfo1(String accountID) throws JSONException
    {
        RequestHandler rq1 = new RequestHandler(this);
        rq1.setRequest(new StatementRequestor());
        try {
            rq1.makeRequest("https://clientaccounts.herokuapp.com/printMini/" + accountID, "GET", null);
        } catch(Exception e) {}

        //rq1.makeRequest("http://dummy.restapiexample.com/api/v1/employees", "GET", null);
    }



    public void getAndDisplayStatementInfo2(String accountID) throws JSONException
    {
        try
        {
            new FillBalance().execute();
        }
        catch(Exception e)
        {
            Log.d("Error", "Exception in FillBalance: " + e.toString());
        }
    }

    private class FillBalance extends AsyncTask<Void, Void, Void>
    {
        String balance;

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                URL url = new URL("https://clientaccounts.herokuapp.com/balance/" + accountID);
                //URL url = new URL("http://dummy.restapiexample.com/api/v1/employees");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try
                {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                    {
                        result.append(line);
                    }
                    Log.d("Bal", result.toString());

                    balance = result.toString();

                    new SendToLogs("https://clientaccounts.herokuapp.com/balance/" + accountID, "", urlConnection.getResponseCode() + "", result.toString()).execute();
                }
                catch (Exception e)
                {
                    Log.d("Error", "Error in URL connection: " + e.toString());
                }
                finally
                {
                    urlConnection.disconnect();
                }
            }
            catch(Exception e)
            {
                Log.d("Error", "Error in malformed URL: " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            balanceText.setBackgroundColor(Color.parseColor("#ff9900"));
            balanceText.setText("CURRENT BALANCE");

            displayBalance.setText("R" + balance + ".00");
        }
    }
}
