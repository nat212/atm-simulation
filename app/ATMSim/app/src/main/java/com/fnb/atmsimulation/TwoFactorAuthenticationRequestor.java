package com.fnb.atmsimulation;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TwoFactorAuthenticationRequestor implements RequestInterface {
    @Override
    public void execute(JSONObject obj, Activity act) {
        Log.d("DATA", obj.toString());
        try {
            String success = obj.getString("success");
            String message = obj.getString("message");

            if(success.equals("true")){
                Intent intent = new Intent( act.getApplicationContext(), HomeActivity.class); // open home activity screen
                intent.putExtra("userID", message); // message is the user id passed to home activity
                act.startActivity(intent);
                act.finish();
            }else{
                if(((InputActivity) act).currentAuth ==2){
                    act.recreate();
                    Toast t = Toast.makeText(act, "Failed to Authenticate, Please Try Again", Toast.LENGTH_LONG); // display error message
                    t.setGravity(Gravity.BOTTOM,0,20);
                    t.show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void execute(JSONArray arr, Activity act) {
        Log.d("DATA", arr.toString());
    }
}
