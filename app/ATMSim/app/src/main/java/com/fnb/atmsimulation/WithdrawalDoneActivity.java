package com.fnb.atmsimulation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WithdrawalDoneActivity extends AppCompatActivity
{
    private String accountID;
    private String accountName;
    //store user id for THIS END!
    private String userID;
    private String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_done);

        //get keys in
        Intent intent = getIntent();
        accountID = intent.getStringExtra("accountID");
        accountName = intent.getStringExtra("accountName");
        //get user id for THIS END!
        userID = intent.getStringExtra("userID");
        Log.d("userID", userID);
        amount = intent.getStringExtra("amount");

        //display money
        Button button = findViewById(R.id.btnTakeMoney);
        button.setText("TAKE R" + amount + ".00");


    }

    public void takeMoney(View view)
    {

        Intent intent = new Intent(this, HomeActivity.class);
        //END! send user id back to home!
        intent.putExtra("userID", userID);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class);
        //END! send user id back to home!
        intent.putExtra("userID", userID);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
