package com.fnb.atmsimulation;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OTPInitialRequest implements RequestInterface {
    @Override
    public void execute(JSONObject obj, Activity act) {

        Log.d("DATA", obj.toString());
        try {
            String success = obj.getString("success");
            String message = obj.getString("message");

            if(success.equals("true")){
                ((InputActivity) act).hideElements();
                ((InputActivity) act).setClientID(message); // set the client ID for later OTP
                ((InputActivity) act).setUpAuthTwo(); // proceed to auth two
            }else{
                Toast.makeText(act, message, Toast.LENGTH_LONG).show(); // display error message
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //not expecting an array
    @Override
    public void execute(JSONArray arr, Activity act) {
        Log.d("DATAarr", arr.toString());
    }
}
