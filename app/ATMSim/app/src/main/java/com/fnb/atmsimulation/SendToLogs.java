package com.fnb.atmsimulation;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.*;
import java.net.*;

public class SendToLogs extends AsyncTask<Void, Void, Void>
{
    String urlOrig;
    String request_body;
    String response_code;
    String response_body;

    public SendToLogs(String urlOrig, String request_body, String response_code, String response_body)
    {

        super();
        this.urlOrig = urlOrig;
        this.request_body = request_body;
        this.response_code = response_code;
        this.response_body = response_body;
        // do stuff
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        try
        {
            URL url = new URL("https://atm.draper.net.za/api/logs");

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try
            {
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("url", urlOrig);
                jsonParam.put("request_body", request_body);
                jsonParam.put("response_code", response_code);
                jsonParam.put("response_body", response_body);

                DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                Log.d("LOGS", String.valueOf(urlConnection.getResponseCode()));
                Log.d("LOGS", urlConnection.getResponseMessage());

                urlConnection.disconnect();


            }
            catch (Exception e)
            {
                Log.d("Error", "Error in POST to Logs: " + e.toString());
            } finally
            {
                urlConnection.disconnect();
            }
        }
        catch (Exception e)
        {
            Log.d("Error", "Error in malformed URL: " + e.toString());
        }

        return null;
    }


}
