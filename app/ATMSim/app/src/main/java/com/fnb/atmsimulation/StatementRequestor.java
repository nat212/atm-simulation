package com.fnb.atmsimulation;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StatementRequestor implements RequestInterface
{

    public void execute(JSONObject obj, Activity act)
    {
        //do nothing
    }
    public void execute(JSONArray arr, Activity act)
    {
        try
        {

            //arr = new JSONArray("[{\"transactionType\":\"deposit\",\"amount\":\"800\",\"date\":\"2019-04-01\", \"time\":\"16:13:41\"},{\"transactionType\":\"withdrawal\",\"amount\":\"600\",\"date\":\"2019-03-01\", \"time\":\"15:49:62\"}]");

            String dateName = "date";
            String timeName = "time";
            String amountName = "amount";
            String typeName = "transactionType";


            TableLayout ll = (TableLayout) act.findViewById(R.id.tableStatement);

            TableRow row = new TableRow(act);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            row.setBackgroundColor(ContextCompat.getColor(act, R.color.fnb_orange));

            int between = 50;
            int sides = 15;

            TextView tv1 = new TextView(act);
            tv1.setText("DATE");
            tv1.setPadding(sides, sides, between, sides);
            row.addView(tv1);

            TextView tv2 = new TextView(act);
            tv2.setText("TIME");
            tv2.setPadding(between, sides, between, sides);
            row.addView(tv2);

            TextView tv3 = new TextView(act);
            tv3.setText("AMOUNT");
            tv3.setPadding(between, sides, sides, sides);
            row.addView(tv3);

            ll.addView(row, 0);


            for (int i = 0; i < arr.length(); i++)
            {
                JSONObject cur = arr.getJSONObject(i);

                row = new TableRow(act);
                lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                tv1 = new TextView(act);
                tv1.setText(cur.getString(dateName));
                tv1.setPadding(sides, sides, between, sides);
                row.addView(tv1);

                tv2 = new TextView(act);
                tv2.setText(cur.getString(timeName));
                tv2.setPadding(between, sides, between, sides);
                row.addView(tv2);

                tv3 = new TextView(act);
                tv3.setText((cur.getString(typeName).equals("deposit") ? "+" : "- ") + " R" + cur.getString(amountName) + ".00");
                tv3.setPadding(between, sides, sides, sides);
                row.addView(tv3);

                ll.addView(row, i+1);
            }
        }
        catch(JSONException e)
        {
            Log.d("Error", "Error in JSON: " + e.toString());
        }
    }
}
