package com.fnb.atmsimulation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    //new check boxes
    private CheckBox ckFacial;
    private CheckBox ckPassword;
    private CheckBox ckOTP;
    private CheckBox ckNFC;
    private CheckBox ckCardNum;



    private Button btnGO;

    private Activity act;


    private int numAuthsSelected;

    private String authOne;
    private String authTwo;

    private ArrayList<CheckBox> checkedItems;
    private ArrayList<CheckBox> allCheckBoxes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        act = this;
        btnGO = findViewById(R.id.btnGO);



        //get check boxes
        ckFacial = findViewById(R.id.ck_facial);
        ckPassword = findViewById(R.id.ck_pass);
        ckOTP = findViewById(R.id.ck_otp);
        ckNFC = findViewById(R.id.ck_nfc_id);
        ckCardNum = findViewById(R.id.ck_card_id);
        checkedItems = new ArrayList<CheckBox>();
        allCheckBoxes = new ArrayList<CheckBox>();

        allCheckBoxes.add(ckCardNum);
        allCheckBoxes.add(ckNFC);
        allCheckBoxes.add(ckOTP);
        allCheckBoxes.add(ckFacial);
        allCheckBoxes.add(ckPassword);


        initialiseSelections();

        //set up listeners
        ckFacial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                @Override
                                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                    Toast.makeText(getApplicationContext(), "facial " + isChecked, Toast.LENGTH_LONG).show();
                                                    refreshSelections();
                                                    disableRelevantBoxes();
                                                }
                                            }
        );


        ckPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                      Toast.makeText(getApplicationContext(), "pin " + isChecked, Toast.LENGTH_LONG).show();
                                                      refreshSelections();
                                                      disableRelevantBoxes();

                                                  }
                                              }
        );

        ckOTP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                             @Override
                                             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                 Toast.makeText(getApplicationContext(), "otp  " + isChecked, Toast.LENGTH_LONG).show();
                                                 refreshSelections();
                                                 disableRelevantBoxes();

                                             }
                                         }
        );

        ckNFC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                             @Override
                                             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                 Toast.makeText(getApplicationContext(), "nfc  " + isChecked, Toast.LENGTH_LONG).show();
                                                 refreshSelections();
                                                 disableRelevantBoxes();

                                             }
                                         }
        );

        ckCardNum.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                 @Override
                                                 public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                     Toast.makeText(getApplicationContext(), "card id  " + isChecked, Toast.LENGTH_LONG).show();
                                                     refreshSelections();
                                                     disableRelevantBoxes();

                                                 }
                                             }
        );


        btnGO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedItems.size() != 2) {
                    Toast.makeText(getApplicationContext(), "Ensure you select two options", Toast.LENGTH_LONG).show();
                    return;
                }

                int authOneIndex = setAuthOne();
                int authTwoIndex = 1 - authOneIndex;
                setAuthTwo(authTwoIndex);

                Intent intent = new Intent(getApplicationContext(), InputActivity.class); // open input screen
                intent.putExtra("authOne", authOne);
                intent.putExtra("authTwo", authTwo);
                startActivity(intent);
                finish(); //dont add to the stack.


            }
        });



    }


    private void refreshSelections() {
        numAuthsSelected = 0;
        checkedItems.clear();
        for (CheckBox box : allCheckBoxes) {
            //loop through all check boxes
            if (box.isChecked()) {
                if (numAuthsSelected < 2) {
                    checkedItems.add(box); //add to the checked items list
                    numAuthsSelected++;
                } else {
                    break;
                }
            }
        }
    }

    private void disableRelevantBoxes() {
        for (CheckBox box : allCheckBoxes) {
            box.setEnabled(true); // assume enabled for below logic
        }

        if (checkedItems.size() == 2) {
            // selected all, therefore disable the rest
            for (CheckBox box : allCheckBoxes) {
                if (!checkedItems.contains(box)) {
                    box.setEnabled(false);  //disable the box. not more than two boxes can be selected
                }
            }
        } else if (checkedItems.size() == 1) {
            CheckBox checkedBox = checkedItems.get(0);
            //now need to disable boxes to guide the user
            if (checkedBox.equals(ckPassword)) {
                for (CheckBox box : allCheckBoxes) {
                    if (!box.equals(ckNFC) && !box.equals(ckCardNum) && !box.equals(ckPassword)) {
                        box.setEnabled(false); //force the user into these options
                    }
                }
            } else if (checkedBox.equals(ckOTP)) {
                ckPassword.setEnabled(false);
            }
        }


    }

    //if the user returns to this screen somehow, it will be reset
    @Override
    protected void onResume() {
        super.onResume();
        initialiseSelections();
    }





    //sets the second authentication method given an index of which box it is
    private void setAuthTwo(int index) {
        CheckBox box = checkedItems.get(index);
        if (box.equals(ckOTP)) {
            authTwo = "OTP";
        } else if (box.equals(ckPassword)) {
            authTwo = "Password";
        } else if (box.equals(ckCardNum)) {
            authTwo = "Card_Num";
        } else if (box.equals(ckFacial)) {
            authTwo = "Facial Recognition";
        } else if (box.equals(ckNFC)) {
            authTwo = "NFC";
        }
    }


    //sets the first authentication method, and returns the index of the first authentication box
    private int setAuthOne() {
        CheckBox box = checkedItems.get(0);
        int index = 0;
        if (box.equals(ckOTP) || box.equals(ckPassword)) {
            box = checkedItems.get(1);
            index = 1;
        }

        if (box.equals(ckCardNum)) {
            authOne = "Card_Num";
            return index;
        } else if (box.equals(ckFacial)) {
            authOne = "Facial Recognition";
            return index;
        } else if (box.equals(ckNFC)) {
            authOne = "NFC";
            return index;
        }
        return -1; //error
    }


    void initialiseSelections() {

        numAuthsSelected = 0;
        authOne = "";
        authTwo = "";
        checkedItems.clear(); //clear checked array
        for (CheckBox box : allCheckBoxes) {
            box.setEnabled(true);
            box.setChecked(false);
        }
    }


}
