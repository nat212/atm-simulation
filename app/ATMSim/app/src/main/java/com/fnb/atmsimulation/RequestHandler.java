package com.fnb.atmsimulation;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.*;
import com.loopj.android.http.*;
import java.util.HashMap;
import cz.msebera.android.httpclient.Header;
/*
*   @class RequestHandler - takes in a generic API request and routes it to the appropriate method in the Requester class.
*/
public class RequestHandler {
    private JSONObject obj;
    private JSONArray arr;
    private Boolean successfulRequest = false;
    private Activity activity;
    private RequestInterface req;

    TextView tView;

    String jsonVal = "";
    private Context cont;

    public RequestHandler(Activity activity)
    {
        this.activity = activity;
    }

    public void setRequest(RequestInterface r)
    {
       req = r;
    }

    /*
    *   @desc takes in a URL and method as well as parameters and sends the actual request to the Requester class which returns the data in a JSON format
    *   @param String url - The API url to which the request will be sent
    *   @param String method - The Request method (GET/POST)
    *   @param HashMap<String, String> data - The hashmap is used to build the RequestParams object that is passed in for post requests. Usually null for GET requests
    *   @returns void
    */
    public void makeRequest(final String url, String method, HashMap<String, String> data) {
        final RequestParams reqParam;
        final String reqParamStringVersion;


        if (data == null)
        {
            reqParam = null;
            reqParamStringVersion = "";
        }
        else
        {
            reqParam = new RequestParams(data);
            reqParamStringVersion = reqParam.toString();
        }
        if (method.toUpperCase() == "GET") {

            Requester.getRequest(url, reqParam, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                   try {
                        
                        successfulRequest = true;
                        new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                        req.execute(res, activity);


                   }catch(Exception e){}
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray res) {

                    successfulRequest = true;
                    new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                    req.execute(res, activity);

                    // Do something with the response
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                    successfulRequest = false;
                }
            });
        } else if (method.toUpperCase() == "POST") {

            Requester.postRequest(url, reqParam, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    try {

                        successfulRequest = true;
                        new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                        req.execute(res, activity);

                    }catch(Exception e){}
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray res) {
                    try {

                        successfulRequest = true;
                        new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                        req.execute(res, activity);

                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                    successfulRequest = false;
                }

            });
        } else if (method.toUpperCase() == "PUT") {

            Requester.putRequest(url, reqParam, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    try {

                        successfulRequest = true;
                        new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                        req.execute(res, activity);

                    }catch(Exception e){}
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray res) {
                    try {
                        
                        successfulRequest = true;
                        new SendToLogs(url, reqParamStringVersion, statusCode+"", res.toString()).execute();
                        req.execute(res, activity);

                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                    successfulRequest = false;
                }
            });
        }
        if(!successfulRequest)
            System.out.println("Request failed");
    }

    //modified makeRequest to deal with nested JSON Objects (overloaded)
       public void makeRequest(final String url, String method, final JSONObject reqParam, Context context) {

        Log.d("reqParam" , reqParam.toString());
        //only worrying about post
        if (method.toUpperCase() == "POST") {

            Requester.postRequest(context, url, reqParam, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    try {
                        new SendToLogs(url, reqParam.toString(), statusCode+"", res.toString()).execute();
                        req.execute(res, activity);
                    }catch(Exception e){}
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray res) {
                    try {

                        new SendToLogs(url, reqParam.toString(), statusCode+"", res.toString()).execute();
                        req.execute(res, activity);

                    } catch (Exception e) {
                    }
                }
            });
        }


    }
}