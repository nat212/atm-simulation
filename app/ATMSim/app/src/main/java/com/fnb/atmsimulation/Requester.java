 package com.fnb.atmsimulation;


import android.content.Context;
import android.util.Log;

import com.loopj.android.http.*;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

 //import org.json.*;
/*
Requester class. Routes get and post requests through to the AsyncHttpClient.

*/
public class Requester {

    private static AsyncHttpClient requestClient = new AsyncHttpClient();

    public Requester() 
    {

    }
    /*   @desc takes in a get request and forwards it through to the AsynchHttpClient.get() function
    *    @param String url - the api URL being requested
    *    @param RequestParams param - parameters required for the request. usually null for GET request
    *    @param AsyncHttpResponseHandler handler - this specifies which format the response should be sent in. JsonHttpResponseHandler/TextHttpResonseHandler etc
    *    @returns void
    */
    public static void getRequest(String url, RequestParams param, AsyncHttpResponseHandler handler)
    {
        requestClient.get(url, param, handler);
    }

    /*   @desc takes in a post request and forwards it through to the AsynchHttpClient.get() function
    *    @param String url - the api URL being requested
    *    @param RequestParams param - parameters required for the request. 
    *    @param AsyncHttpResponseHandler handler - this specifies which format the response should be sent in. JsonHttpResponseHandler/TextHttpResonseHandler etc
    *    @returns void
    */
    public static void postRequest(String url, RequestParams param, AsyncHttpResponseHandler handler)
    {
        requestClient.post(url, param, handler);
    }

    //modification for post Request with json object
    public static void postRequest(Context context, String url, JSONObject param, AsyncHttpResponseHandler handler)
    {
        try {
            StringEntity entity = new StringEntity(param.toString());
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            Log.d("reqFIN" , "url " + url + " param " + entity + " orig json " + param);
            requestClient.post(context, url, entity, "application/json", handler);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /*   @desc takes in a post request and forwards it through to the AsynchHttpClient.get() function
     *    @param String url - the api URL being requested
     *    @param RequestParams param - parameters required for the request.
     *    @param AsyncHttpResponseHandler handler - this specifies which format the response should be sent in. JsonHttpResponseHandler/TextHttpResonseHandler etc
     *    @returns void
     */
    public static void putRequest(String url, RequestParams param, AsyncHttpResponseHandler handler)
    {
        requestClient.put(url, param, handler);
    }

}