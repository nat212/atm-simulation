package com.fnb.atmsimulation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity {

    //Jared added this
    private String userID;
    private ExpandableAdapter expandableAdapter;
    private ExpandableListView expList;
    private String[] parents;
    private String[] parToAccId;
    public static ArrayList<ArrayList<String>> childList;
    Activity homeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Jared added this, gets user id passed from login
        userID = getIntent().getStringExtra("userID");

        switch (userID){
            case "1821":{
                userID = "562";
            }
            break;
            case "1831":{
                userID = "569";
            }
            break;
            case "1841":{
                userID = "300";

            }
            break;
            case "1851":{
                userID = "111";

            }
            break;
            case "1861":{
                userID = "123";

            }
            break;
            default:
            {
                userID = "562";
            }
        }
        Log.d("userID", userID);


        homeActivity= this;



        fetchRemoteAccounts();
//        setDefaultAccounts();




    }


    private void fetchRemoteAccounts(){
        RequestHandler rq = new RequestHandler(this);
        rq.setRequest(new AccountRequester());
        rq.makeRequest("https://clientaccounts.herokuapp.com/getAccounts/" + userID, "GET", new HashMap<String, String>());
    }

    public void setUpExpandableList(){
        expList = (ExpandableListView) findViewById(R.id.exp_list);
        expandableAdapter = new ExpandableAdapter(this, childList, parents);
        expList.setAdapter(expandableAdapter);
        expList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandablelistview,
                                        View clickedView, int groupPosition, int childPosition, long childId) {
                if(childPosition ==0){
                    //Statement
//                    Toast.makeText(getApplicationContext(), "" + parToAccId[groupPosition] + " child " + parents[groupPosition] , Toast.LENGTH_LONG).show();

                    openStatement(parToAccId[groupPosition] ,parents[groupPosition]);
                }else if(childPosition ==1){
                    //withdrawal
//                    Toast.makeText(getApplicationContext(), "" + parToAccId[groupPosition] + " child " + parents[groupPosition] , Toast.LENGTH_LONG).show();

                    openWithdrawal(parToAccId[groupPosition] ,parents[groupPosition]);
                }

//                Toast.makeText(getApplicationContext(), "" + groupPosition + " child " + childPosition , Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }

    //dummy accounts used for testing
    private void setDefaultAccounts(){
        String tmpParents[] = new String[] {"Cheque", "Savings"};
        String tmpAcc[] = new String[] {"5", "7"};
        setParentArray(tmpParents, tmpAcc);
        ArrayList<String> tmp;
        childList = new ArrayList<>();
        for(int i =0; i < parents.length; i++){
            tmp = new ArrayList<>();
            addChildren(tmp);
            childList.add(tmp);
        }
    }

    //sets the parent array from the list of a ccounts and also makes a corresponding account ID array
    public void setParentArray(String []par, String[] accIdArr){
        parents = par.clone();
        parToAccId = accIdArr.clone();
        Log.d("parents", "setParentArray: " + parents.length + "  accid " + parToAccId.length);
    }

    public void setChildList(ArrayList<ArrayList<String>> arr){
        Log.d("children", arr.toString());

        childList = arr;
    }

    //adds default children
    private void addChildren(ArrayList<String> list){
        list.add("Statement");
        list.add("Withdrawal");
    }

    //forcing if back pressed here to log out and go back to main screen. Done to skip the OTP screen in between.
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void openStatement(String accId, String accType)
    {
        Intent intent = new Intent(this, StatementActivity.class);
        intent.putExtra("accountID", accId);
        intent.putExtra("accountName", accType);
        startActivity(intent);
    }

    public void openWithdrawal(String accId, String accType)
    {
        Intent intent = new Intent(this, WithdrawalAmountActivity.class);
        intent.putExtra("accountID", accId);
        intent.putExtra("accountName", accType);
        //Jared added this, sends userID along to withdrawal for when it ends
        intent.putExtra("userID", userID);
        startActivity(intent);
    }

}
