package com.fnb.atmsimulation;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AccountRequester implements RequestInterface {
    @Override
    public void execute(JSONObject obj, Activity act) {

        Log.d("DATA", obj.toString());

    }

    //not expecting an array
    @Override
    public void execute(JSONArray arr, Activity act) {
        Log.d("DATAarr", arr.toString());
        // now we have an array of accouts
        if (arr.length() > 0) {
            try {
                String tmpParents[] = new String[arr.length()];
                String tmpAcc[] = new String[arr.length()];
                JSONObject o;

                 ArrayList<ArrayList<String>> childList = new ArrayList<>();
                ArrayList<String> child;

                for (int i = 0; i < arr.length(); i++) {
                    o = arr.getJSONObject(i);
                    tmpAcc[i] = "" + o.getInt("accountID");
                    tmpParents[i] = o.getString("accountType");
                    child = new ArrayList<>();
                    child.add("Statement");
                    child.add("Withdrawal");
                    childList.add(child); //add to the arraylist of arraylists
                }
                ((HomeActivity) act).setParentArray(tmpParents, tmpAcc);
                ((HomeActivity) act).setChildList(childList);
                ((HomeActivity) act).setUpExpandableList();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private void addChildren(ArrayList<String> list){
        list.add("Statement");
        list.add("Withdrawal");
    }
}
