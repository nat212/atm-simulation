package com.fnb.atmsimulation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class WithdrawalAmountActivity extends AppCompatActivity
{

    private String accountID;
    private String accountName;
    //store user id for end
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_amount);

        //get keys in
        Intent intent = getIntent();
        accountID = intent.getStringExtra("accountID");
        accountName = intent.getStringExtra("accountName");
        //get user id for end
        userID = intent.getStringExtra("userID");
        Log.d("userID", userID);

        //display heading
        TextView heading = findViewById(R.id.withdrawalHeading);
        heading.setText("Withdrawal from " + accountName);


    }

    public void confirmAmount(View view)
    {


        Intent intent = new Intent(this, WithdrawalConfirmActivity.class);

        //get amount entered
        EditText editText = (EditText) findViewById(R.id.amount);
        String message = editText.getText().toString();

        //store keys
        intent.putExtra("accountID", accountID);
        intent.putExtra("accountName", accountName);
        //send user id for end
        intent.putExtra("userID", userID);
        intent.putExtra("amount", message);

        //go to confirm withdrawal
        startActivity(intent);
    }
}
