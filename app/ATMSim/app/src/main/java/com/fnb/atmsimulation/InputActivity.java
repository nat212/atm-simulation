package com.fnb.atmsimulation;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import cz.msebera.android.httpclient.protocol.HTTP;

public class InputActivity extends AppCompatActivity {
    private String authOne;
    private String authTwo;
    private String authOneData;
    private String authTwoData;
    private EditText editField;
    private TextView txtView;
    private Button btnGo;
    public int currentAuth;
    private String facialImage;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String clientID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        editField = findViewById(R.id.txtEditField);
        txtView = findViewById(R.id.txtViewPrompt);
        btnGo = findViewById(R.id.btnGoInput);
        currentAuth = -1;
        hideElements();

        Intent intent = getIntent();
        authOne = intent.getStringExtra("authOne");
        authTwo = intent.getStringExtra("authTwo"); //fetch the selected auth methods


        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = editField.getText().toString();
                if (data.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Something!", Toast.LENGTH_LONG).show();

                } else {
                    if (currentAuth == 1) {
                        authOneData = data; //set data
                        if (authTwo.equals("OTP")) {
                            //deal with OTP by sending initial request
//                            Toast.makeText(getApplicationContext(), "Dealing with OTP!", Toast.LENGTH_LONG).show();
                            authenticateInitialForOTP(); // the request handler will worry about hiding elements etc
                        }else{
                            hideElements();
                            setUpAuthTwo();
                        }


                    } else if (currentAuth == 2) {
                        if (authTwo.equals("OTP")) {
                            //send a custom request with user ID returned and data (OTP)

//                            Toast.makeText(getApplicationContext(), "making secondary OTP request!", Toast.LENGTH_LONG).show();
                            authTwoData = data; // setting data for auth two
                            authenticateSecondPassWithOTP();
                        } else {
                            authTwoData = data;
                            //now make final request with authOneData and authTwoData
//                            Toast.makeText(getApplicationContext(), "making final request!", Toast.LENGTH_LONG).show();
                            authenticateWithTwoFactors();
                        }

                    }
                }


            }

        });


        setUpAuthOne();// start with first authentication

    }

    public void setUpAuthOne() {
        currentAuth = 1;
        switch (authOne) {
            case "NFC": {
                txtView.setText("Please Enter Your NFC ID");
                editField.setHint("NFC ID");
                showElements();
            }
            break;
            case "Card_Num": {
                txtView.setText("Please Enter Your Card Number");
                editField.setHint("Card Number");
                showElements();
            }
            break;
            case "Facial Recognition": {
                setPicture();
                setUpAuthTwo(); // this will take user to second stage as there is not onclick for go button
            }
            break;

        }

    }

    public void setUpAuthTwo() {
        currentAuth = 2;

        switch (authTwo) {
            case "NFC": {
                txtView.setText("Please Enter Your NFC ID");
                editField.setHint("NFC ID");
                showElements();
            }
            break;
            case "Card_Num": {
                txtView.setText("Please Enter Your Card Number");
                editField.setHint("Card Number");
                showElements();
            }
            break;
            case "Facial Recognition": {
                setPicture();
            }
            break;
            case "Password": {
                txtView.setText("Please Enter Your Password");
                editField.setHint("Password");
                showElements();
            }
            break;
            case "OTP": {
                txtView.setText("Please Enter Your One Time Pin");
                editField.setHint("OTP");
                showElements();
            }
            break;

        }

    }

    private void pictureCallback() {
        if (currentAuth == 1) {
            authOneData = facialImage;
        } else if (currentAuth == 2 && authTwo.equals("Facial Recognition")) {
            authTwoData = facialImage;
            //here we are done with 2 params
            authenticateWithTwoFactors();
        }
    }

    private void authenticateInitialForOTP(){
        JSONObject mainObject = new JSONObject();
        JSONObject auth1 = new JSONObject();
        JSONObject auth2 = new JSONObject();
        try {
            auth1.put("type", authOne);

            auth1.put("data", authOneData); //authentication one

            auth2.put("type", authTwo);
            auth2.put("data", ""); // authentication two data is blank for OTP
            mainObject.put("AuthOne", auth1);
            mainObject.put("AuthTwo", auth2);

            RequestHandler rq = new RequestHandler(this);
            rq.setRequest(new OTPInitialRequest());
            rq.makeRequest("https://mini-project-authentication.herokuapp.com/api/authenticate", "POST", mainObject, getApplicationContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void authenticateWithTwoFactors() {
        //here send out final request to determine whether to go ahead or not
        JSONObject mainObject = new JSONObject();
        JSONObject auth1 = new JSONObject();
        JSONObject auth2 = new JSONObject();
        try {
        auth1.put("type", authOne);

            auth1.put("data", authOneData); //authentication one

        auth2.put("type", authTwo);
        auth2.put("data", authTwoData); // authentication two
        mainObject.put("AuthOne", auth1);
        mainObject.put("AuthTwo", auth2);

        RequestHandler rq = new RequestHandler(this);
        rq.setRequest(new TwoFactorAuthenticationRequestor());
        rq.makeRequest("https://mini-project-authentication.herokuapp.com/api/authenticate", "POST", mainObject, getApplicationContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Intent intent = new Intent(getApplicationContext(), HomeActivity.class); // open input screen
//        intent.putExtra("userID", "12345");
//        startActivity(intent);
//        finish();
    }

    public void setClientID(String cid){
        clientID = cid;
    }

    private void authenticateSecondPassWithOTP() {
        //send request to auth and get response

        JSONObject mainObject = new JSONObject();
        JSONObject auth1 = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            auth1.put("type", authTwo); // this is OTP acting as authOne

            data.put("clientID" , clientID);// this was received from first pass
            data.put("Password", authTwoData);
            auth1.put("data", data); //authentication one

            mainObject.put("AuthOne", auth1);

            RequestHandler rq = new RequestHandler(this);
            rq.setRequest(new TwoFactorAuthenticationRequestor()); // can reuse this requester as the functionality will be the same
            rq.makeRequest("https://mini-project-authentication.herokuapp.com/api/authenticate", "POST", mainObject, getApplicationContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //this function organises permissions then proceeds to take a photo and encode it as base64. when this is complete a callback is called
    private void setPicture() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    1);

        } else {
            //permission granted
            facialImage = "base64failureimage";
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setPicture();
                } else {
                    finish();
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            facialImage = Base64.encodeToString(byteArray, Base64.DEFAULT);     //set the image to global var
            pictureCallback(); //done with image
            Log.d("image", facialImage);
        }
    }

    public void hideElements() {
        editField.setVisibility(View.GONE);
        editField.setText("");
        txtView.setVisibility(View.GONE);
        btnGo.setVisibility(View.GONE);
    }

    private void showElements() {
        editField.setVisibility(View.VISIBLE);
        txtView.setVisibility(View.VISIBLE);
        btnGo.setVisibility(View.VISIBLE);
    }


}
