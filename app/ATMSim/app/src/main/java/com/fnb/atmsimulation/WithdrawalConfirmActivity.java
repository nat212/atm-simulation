package com.fnb.atmsimulation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

public class WithdrawalConfirmActivity extends AppCompatActivity
{

    private String accountID;
    private String accountName;
    private String userID;
    private String amount;
    private Activity act;

    public String getAccountID()
    {
        return accountID;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public String getUserID()
    {
        return userID;
    }

    public String getAmount()
    {
        return amount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_confirm);

        Intent intent = getIntent();
        accountID = intent.getStringExtra("accountID");
        accountName = intent.getStringExtra("accountName");
        userID = intent.getStringExtra("userID");
        amount = intent.getStringExtra("amount");

        act = this;

        TextView dspAccount = findViewById(R.id.dspAccount);
        dspAccount.setText(accountName);

        TextView dspAmount = findViewById(R.id.dspAmount);
        dspAmount.setText("R" + amount + ".00");
    }

    public void confirmWithdrawal(View view)
    {
        try
        {
            new DoWithdrawal().execute();
        }
        catch (Exception e)
        {
            Log.d("Error", "Exception in DoWithdrawal: " + e.toString());
        }


    }

    private class DoWithdrawal extends AsyncTask<Void, Void, Void>
    {
        String result;

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {

                HttpClient client = HttpClientBuilder.create().build();
                HttpPut httpPut = new HttpPut("https://clientaccounts.herokuapp.com/withdraw/" + accountID + "/" + amount);

                HttpResponse resp = client.execute(httpPut);
                String responseString = new BasicResponseHandler().handleResponse(resp);

                result = responseString;

                new SendToLogs("https://clientaccounts.herokuapp.com/withdraw/" + accountID + "/" + amount, "", resp.getStatusLine().getStatusCode()+"", result).execute();
            }
            catch (Exception e)
            {
                Log.d("Error", "Error in IO: " + e.toString());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            String status = result;
            if (status.startsWith("Success"))
            {
                Intent intent = new Intent(act, WithdrawalDoneActivity.class);

                intent.putExtra("accountID", ((WithdrawalConfirmActivity) act).getAccountID());
                intent.putExtra("accountName", ((WithdrawalConfirmActivity) act).getAccountName());
                intent.putExtra("userID", ((WithdrawalConfirmActivity) act).getUserID());
                intent.putExtra("amount", ((WithdrawalConfirmActivity) act).getAmount());

                act.startActivity(intent);
            }
            else
            {
                Toast.makeText(act, "Insufficient funds!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
