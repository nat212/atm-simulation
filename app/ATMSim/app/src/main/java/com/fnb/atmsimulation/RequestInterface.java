package com.fnb.atmsimulation;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONObject;

public interface RequestInterface {
    void execute(JSONObject obj, Activity act);
    void execute(JSONArray arr, Activity act);
}



