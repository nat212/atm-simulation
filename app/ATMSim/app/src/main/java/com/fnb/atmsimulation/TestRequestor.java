package com.fnb.atmsimulation;

import android.app.Activity;
import android.widget.Toast;

import org.json.*;

public class TestRequestor implements RequestInterface {
    public void execute(JSONObject obj, Activity act)
    {
        try
        {
            StringBuilder str = new StringBuilder();
            str.append("Name: " + obj.getString("name") + "\n");
            str.append("Username: " + obj.getString("username") + "\n");
            str.append("Email: " + obj.getString("email") + "\n");
            str.append("Phone: " + obj.getString("phone") + "\n");
            str.append("Website: " + obj.getString("website") + "\n");
            JSONObject temp = obj.getJSONObject("address");
            str.append("Street:" + temp.getString("street") + "\n");


            Toast.makeText(act, str.toString(), Toast.LENGTH_LONG).show();
        }catch(Exception e)
        {

        }

    }

    public void execute(JSONArray arr, Activity act)
    {
        try {
            JSONObject first = (JSONObject) arr.get(0);
            execute(first, act);


        } catch (Exception e) {}
    }
}
