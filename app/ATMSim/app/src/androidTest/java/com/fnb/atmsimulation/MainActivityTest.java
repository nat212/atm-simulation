package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureButtonIsPresent() throws Exception {
        MainActivity activity = rule.getActivity();
        View viewById = activity.findViewById(R.id.btnGO);
        assertThat(viewById,notNullValue());
        assertThat(viewById, instanceOf(Button.class));

    }

    @Test
    public void ensureCheckboxIsPresent() throws Exception {
        MainActivity activity = rule.getActivity();
        View card = activity.findViewById(R.id.ck_card_id);
        assertThat(card,notNullValue());
        assertThat(card, instanceOf(CheckBox.class));

        View nfc = activity.findViewById(R.id.ck_nfc_id);
        assertThat(nfc,notNullValue());
        assertThat(nfc, instanceOf(CheckBox.class));

        View face = activity.findViewById(R.id.ck_facial);
        assertThat(face,notNullValue());
        assertThat(face, instanceOf(CheckBox.class));

        View otp = activity.findViewById(R.id.ck_otp);
        assertThat(otp,notNullValue());
        assertThat(otp, instanceOf(CheckBox.class));

        View pwd = activity.findViewById(R.id.ck_pass);
        assertThat(pwd,notNullValue());
        assertThat(pwd, instanceOf(CheckBox.class));


    }
}
