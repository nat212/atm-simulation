package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class WithdrawalConfirmActivityTest {
    @Rule
    public ActivityTestRule<WithdrawalConfirmActivity> rule  = new  ActivityTestRule<WithdrawalConfirmActivity>(WithdrawalConfirmActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("accountID", "1");
            intent.putExtra("accountName", "Savings");
            intent.putExtra("userID", "12345");
            intent.putExtra("amount", "500");
            return intent;
        }
    };

    @Test
    public void ensureTextViewIsPresent() throws Exception {
        WithdrawalConfirmActivity activity = rule.getActivity();
        View withdraw = activity.findViewById(R.id.confirmHeading);
        assertThat(withdraw,notNullValue());
        assertThat(withdraw, instanceOf(TextView.class));


        View withdrawInst = activity.findViewById(R.id.textView2);
        assertThat(withdrawInst,notNullValue());
        assertThat(withdrawInst, instanceOf(TextView.class));

        View acc = activity.findViewById(R.id.lblAccount);
        assertThat(acc,notNullValue());
        assertThat(acc, instanceOf(TextView.class));

        View amnt = activity.findViewById(R.id.lblAmount);
        assertThat(amnt,notNullValue());
        assertThat(amnt, instanceOf(TextView.class));


        View dspAcc = activity.findViewById(R.id.dspAccount);
        assertThat(dspAcc,notNullValue());
        assertThat(dspAcc, instanceOf(TextView.class));

        View dspAmnt = activity.findViewById(R.id.dspAmount);
        assertThat(dspAmnt,notNullValue());
        assertThat(dspAmnt, instanceOf(TextView.class));


    }


    @Test
    public void ensureButtonIsPresent() throws Exception {
        WithdrawalConfirmActivity activity = rule.getActivity();
        View btnAmnt = activity.findViewById(R.id.confirmButton);
        assertThat(btnAmnt,notNullValue());
        assertThat(btnAmnt, instanceOf(Button.class));

    }


}
