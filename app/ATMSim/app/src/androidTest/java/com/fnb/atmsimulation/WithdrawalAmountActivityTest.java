package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class WithdrawalAmountActivityTest {
    @Rule
    public ActivityTestRule<WithdrawalAmountActivity> rule  = new  ActivityTestRule<WithdrawalAmountActivity>(WithdrawalAmountActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("accountID", "1");
            intent.putExtra("accountName", "Savings");
            intent.putExtra("userID", "12345");
            return intent;
        }
    };

    @Test
    public void ensureTextViewIsPresent() throws Exception {
        WithdrawalAmountActivity activity = rule.getActivity();
        View withdraw = activity.findViewById(R.id.withdrawalHeading);
        assertThat(withdraw,notNullValue());
        assertThat(withdraw, instanceOf(TextView.class));


        View withdrawInst = activity.findViewById(R.id.instr);
        assertThat(withdrawInst,notNullValue());
        assertThat(withdrawInst, instanceOf(TextView.class));


    }

    @Test
    public void ensureEditTextIsPresent() throws Exception {
        WithdrawalAmountActivity activity = rule.getActivity();
        View amnt = activity.findViewById(R.id.amount);
        assertThat(amnt,notNullValue());
        assertThat(amnt, instanceOf(EditText.class));

    }

    @Test
    public void ensureButtonIsPresent() throws Exception {
        WithdrawalAmountActivity activity = rule.getActivity();
        View btnAmnt = activity.findViewById(R.id.confirmAmount);
        assertThat(btnAmnt,notNullValue());
        assertThat(btnAmnt, instanceOf(Button.class));

    }


}
