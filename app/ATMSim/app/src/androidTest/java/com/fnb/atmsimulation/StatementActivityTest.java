package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class StatementActivityTest {
    @Rule
    public ActivityTestRule<StatementActivity> rule  = new  ActivityTestRule<StatementActivity>(StatementActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("accountID", "1");
            intent.putExtra("accountName", "Savings");
            return intent;
        }
    };

    @Test
    public void ensureTextViewIsPresent() throws Exception {
        StatementActivity activity = rule.getActivity();
        View bal = activity.findViewById(R.id.balanceText);
        assertThat(bal,notNullValue());
        assertThat(bal, instanceOf(TextView.class));

        View disp = activity.findViewById(R.id.displayBalance);
        assertThat(disp,notNullValue());
        assertThat(disp, instanceOf(TextView.class));


    }
}
