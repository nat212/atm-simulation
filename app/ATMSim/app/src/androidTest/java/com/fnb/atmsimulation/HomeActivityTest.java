package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class HomeActivityTest {
    @Rule
    public ActivityTestRule<HomeActivity> rule  = new  ActivityTestRule<HomeActivity>(HomeActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("userID", "12345");
            return intent;
        }
    };

    @Test
    public void ensureListViewIsPresent() throws Exception {
        HomeActivity activity = rule.getActivity();
        View btnWithdraw1 = activity.findViewById(R.id.exp_list);
        assertThat(btnWithdraw1,notNullValue());
        assertThat(btnWithdraw1, instanceOf(ExpandableListView.class));

    }
}
