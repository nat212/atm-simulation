package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class WithdrawalDoneActivityTest {
    @Rule
    public ActivityTestRule<WithdrawalDoneActivity> rule  = new  ActivityTestRule<WithdrawalDoneActivity>(WithdrawalDoneActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("accountID", "1");
            intent.putExtra("accountName", "Savings");
            intent.putExtra("userID", "12345");
            intent.putExtra("amount", "0");
            return intent;
        }
    };

    @Test
    public void ensureTextViewIsPresent() throws Exception {
        WithdrawalDoneActivity activity = rule.getActivity();
        View withdraw = activity.findViewById(R.id.lblSuccess);
        assertThat(withdraw,notNullValue());
        assertThat(withdraw, instanceOf(TextView.class));


    }


    @Test
    public void ensureButtonIsPresent() throws Exception {
        WithdrawalDoneActivity activity = rule.getActivity();
        View btnAmnt = activity.findViewById(R.id.btnTakeMoney);
        assertThat(btnAmnt,notNullValue());
        assertThat(btnAmnt, instanceOf(Button.class));

    }

    @Test
    public void ensureImageViewIsPresent() throws Exception {
        WithdrawalDoneActivity activity = rule.getActivity();
        View imgLogo = activity.findViewById(R.id.imageView);
        assertThat(imgLogo,notNullValue());
        assertThat(imgLogo, instanceOf(ImageView.class));

    }




}
