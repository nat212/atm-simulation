package com.fnb.atmsimulation;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;


import android.content.Intent;
import android.renderscript.ScriptGroup;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.*;
import android.support.test.rule.*;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.*;
import android.widget.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class InputActivityTest {
    @Rule
    public ActivityTestRule<InputActivity> rule  = new  ActivityTestRule<InputActivity>(InputActivity.class){
        @Override
        protected Intent getActivityIntent() {
            InstrumentationRegistry.getTargetContext();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.putExtra("authOne", "NFC");
            intent.putExtra("authTwo", "Password");
            return intent;
        }
    };

    @Test
    public void ensureButtonIsPresent() throws Exception {
        InputActivity activity = rule.getActivity();
        View btnGo = activity.findViewById(R.id.btnGoInput);
        assertThat(btnGo,notNullValue());
        assertThat(btnGo, instanceOf(Button.class));

    }

    @Test
    public void ensureEditTextIsPresent() throws Exception {
        InputActivity activity = rule.getActivity();
        View edtText = activity.findViewById(R.id.txtEditField);
        assertThat(edtText,notNullValue());
        assertThat(edtText, instanceOf(EditText.class));

    }

    @Test
    public void ensureTextViewIsPresent() throws Exception {
        InputActivity activity = rule.getActivity();
        View txtV = activity.findViewById(R.id.txtViewPrompt);
        assertThat(txtV,notNullValue());
        assertThat(txtV, instanceOf(TextView.class));

    }
}
