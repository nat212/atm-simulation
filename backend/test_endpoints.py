from aiohttp import web
from endpoints import get_logs, post_log
import pytest
import json
from config import Config
from db import MongoClient
import urllib
from datetime import datetime, timedelta


conf = Config()
MongoClient(conf.get_dbhost(), conf.get_dbport(), 'logs_test')


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_get('/api/logs', get_logs)
    app.router.add_post('/api/logs', post_log)
    return loop.run_until_complete(aiohttp_client(app))


async def test_post(cli):
    resp = await cli.post('/api/logs', json={'url': 'example.com',
                                             'request_body': {'test': 'value'},
                                             'response_code': 200,
                                             'response_body': {'succ': True}
                                             })
    assert resp.status == 201
    data = json.loads(await resp.text())
    assert 'timestamp' in data
    assert data['url'] == 'example.com'
    assert data['request_body']['test'] == 'value'
    assert data['response_code'] == 200
    assert data['response_body']['succ'] is True


async def test_get(cli):
    url = '/api/logs?%s' % urllib.parse.urlencode({
        'startDate': datetime.now() - timedelta(days=2),
        'endDate': datetime.now() + timedelta(days=2),
        })
    resp = await cli.get(url)
    data = json.loads(await resp.text())
    assert len(data) >= 1
    assert 'url' in data[0]
    assert 'request_body' in data[0]
    assert 'response_code' in data[0]
    assert 'response_body' in data[0]
    assert 'sent' not in data[0]
    assert 'timestamp' in data[0]
