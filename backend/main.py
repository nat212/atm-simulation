from config import Config
from db import MongoClient
from aiohttp.web import Application, run_app
import aiohttp_cors
from endpoints import get_logs, post_log, health_check
from aiohttp_swagger import setup_swagger

conf = Config()
MongoClient(conf.get_dbhost(), conf.get_dbport(), 'logs')

app = Application()
cors = aiohttp_cors.setup(app, defaults={
    '*': aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers='*',
        allow_headers='*',
    )
})

resource = cors.add(app.router.add_resource('/api/logs'))
cors.add(resource.add_route('GET', get_logs))
cors.add(resource.add_route('POST', post_log))

status_resource = cors.add(app.router.add_resource('/'))
cors.add(status_resource.add_route('GET', health_check))

setup_swagger(app, title="ATM Logs Backend", api_version="1.0.0")

if __name__ == '__main__':
    run_app(app, port=conf.get_port())
