from aiohttp.web import Request, Response
from db import MongoClient
import json
import datetime
from dateutil import parser
import requests
import time

COLLECTION_NAME = 'logs'
REPORTING_URL = 'https://fnbreports-6455.nodechef.com/api'


def params_check(expected, given):
    missing_params = set(expected) - set(given)
    extra_params = set(given) - set(expected)

    return missing_params, extra_params


async def get_logs(request: Request) -> Response:
    """
    ---
    description: This endpoint allows retrieving of logs in a given timeframe
    tags:
    - Logs API
    summary: Get Logs
    produces:
    - text/plain
    - application/json
    parameters:
    - in: query
      name: startDate
      description: The starting time to filter logs with
      schema:
       type: string
       format: date-time
    - in: query
      name: endDate
      description: The ending time to filter logs with
      schema:
       type: string
       format: date-time
    responses:
        "200":
            description: Logs were successfully found
        "404":
            description: There were no logs in the time span
        "422":
            description: Either an invalid parameter was given,
                         or a parameter was missing
    """
    query = request.query
    missing_params, extra_params = params_check(['startDate', 'endDate'],
                                                query.keys())
    if missing_params:
        return Response(status=422, body=('Missing parameters '
                        f'{", ".join(missing_params)}'),
                        content_type='text/plain')
    if extra_params:
        return Response(status=422, body=('Invalid parameters '
                        f'{", ".join(extra_params)}'),
                        content_type='text/plain')

    start = parser.parse(query['startDate'])
    end = parser.parse(query['endDate'])

    logs = []
    try:
        logs = list(MongoClient.find(COLLECTION_NAME, {
            'timestamp': {'$gte': start, '$lte': end},
        }, {'_id': 0}))
        for log in logs:
            if 'sent' in log:
                del log['sent']
            log['timestamp'] = log['timestamp'].isoformat()
    except Exception:
        return Response(status=500, body='There was an error fetching logs',
                        content_type='text/plain')
    if not logs:
        return Response(status=404, body='There were no logs found',
                        content_type='text/plain')
    return Response(status=200, body=json.dumps(list(logs)),
                    content_type='application/json')


async def post_log(request: Request) -> Response:
    """
    ---
    description: This endpoint allows the creation of a log
    tags:
    - Logs API
    summary: Post Log
    produces:
    - application/json
    - text/plain
    parameters:
    - in: body
      name: body
      description: A JSON object describing a request that was made
      schema:
        type: object
        properties:
          url:
            type: string
          request_body:
            type: object
          response_code:
            type: integer
            format: int32
          response_body:
            type: object
    responses:
        "201":
            description: The log was created successfully
        "422":
            description: Either an invalid parameter was given,
                         or a parameter was missing
        "500":
            description: An error occurred while saving the log to the database
    """
    data = await request.json()
    missing_params, extra_params = params_check(set(['url', 'request_body',
                                                'response_code',
                                                    'response_body']),
                                                data.keys())
    if missing_params:
        return Response(status=422, body=('Missing parameters '
                        f'{", ".join(missing_params)}'),
                        content_type='text/plain')
    if extra_params:
        return Response(status=422, body=('Invalid parameters '
                        f'{", ".join(extra_params)}'),
                        content_type='text/plain')

    data['timestamp'] = datetime.datetime.now()
    data['sent'] = False

    try:
        MongoClient.insert(COLLECTION_NAME, data)
        if '_id' in data:
            del data['_id']
        if 'sent' in data:
            del data['sent']
        data['timestamp'] = data['timestamp'].isoformat()
    except Exception as ex:
        print(ex)
        return Response(status=500, body='There was an error creating the log',
                        content_type='text/plain')
    if MongoClient.count(COLLECTION_NAME, {'sent': False}) >= 20:
        to_send = list(MongoClient.find(COLLECTION_NAME, {'sent': False},
                                        {'_id': 0}))
        for log in to_send:
            if 'timestamp' in log:
                log['timestamp'] = time.mktime(log['timestamp'].timetuple()) \
                        * 1000 + log['timestamp'].microsecond / 1000
            if 'sent' in log:
                del log['sent']
        headers = {'Content-type': 'application/x-www-form-urlencoded'}
        data = {'system': 'ATMSS', 'data': json.dumps(to_send)}
        r = requests.post(REPORTING_URL, headers=headers, data=data)
        MongoClient.update(COLLECTION_NAME, {'sent': False}, {'sent': True})
        print(r.text)

    return Response(status=201, body=json.dumps(data),
                    content_type='application/json')


async def health_check(request):
    """
    ---
    description: Checks the health of the application
    tags:
    - Health check
    produces:
    - text/plain
    responses:
        "200":
            description: Server is healthy
        "500":
            description: Server is unhealthy
    """
    return Response(status=200, body='OK', content_type='text/plain')
